# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import std/[asyncdispatch, sequtils, os, streams, uri]
import preserves, preserves/jsonhooks, syndicate
from syndicate/protocols/dataspace import Observe
import eris, eris/composite_stores, eris/url_stores
import freedesktop_org
import ./protocol

type Observe = dataspace.Observe[Cap]

proc immutulate(turn: var Turn; ds: Cap; store: ErisStore; filePath: string, cacheStream: Stream) =
  if fileExists(filePath):
    stderr.writeLine "immutulate ", filePath
    let
      fileSize = filePath.getFileInfo.size
      mimeTypes = mimeTypeOf(filePath)
      fileStream = openFileStream(filePath)
      chunkSize = recommendedChunkSize(fileSize)
      ingest = newErisIngest(store, chunkSize, convergentMode)
    var mimeType: string
    if mimeTypes.len > 0:
      mimeType = mimeTypes[0]
    append(ingest, fileStream).addCallback(turn) do (turn: var Turn):
      ingest.cap.addCallback(turn) do (turn: var Turn; ec: ErisCap):
        var rec = initRecord(
            "immutulation",
            filePath.toPreserve(Cap),
            ($ec).toPreserve(Cap),
            fileSize.toPreserve(Cap),
            mimeType.toSymbol(Cap),
          )
        discard publish(turn, ds, rec)
        cacheStream.writeLine($rec)
        cacheStream.flush()
  else:
    stderr.writeLine "cannot immutulate file that does not exist: ", filePath

runActor("immutulator") do (root: Cap; turn: var Turn):
  connectStdio(root, turn)
  during(turn, root, ?BootArgs) do (cachefile: string, ds: Cap, stores: seq[string]):
    let cacheStream = openFileStream(cachefile, fmAppend)

    let store = newMultiStore()
    if stores.len == 0: store.add(newDiscardStore())
    else:
      for s in stores:
        let u = parseUri s
        let c = waitFor newStoreClient(u)
        stderr.writeLine "connected to ", u
        store.add(c)

    let pat = ?Observe(pattern: !Immutulation) ?? {0: grabLit()}
    during(turn, ds, pat) do (filePath: string):
      immutulate(turn, ds, store, filePath, cacheStream)

  do:
    close(store)
