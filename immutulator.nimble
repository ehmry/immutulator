bin = @["immutulator"]
license = "Unlicense"
requires: "nim", "syndicate"
srcDir = "src"
version = "20230905"
