# The Immutulator

Ingest files into ERIS.

It takes a bit of configuring. An example:
```
let ?ds = dataspace
<eris $ds>
<bind <ref {oid: "eris" key: #x""}> $ds #f>

<daemon immutulator {
  argv: ["/bin/immutulator"]
  protocol: application/syndicate
}>

? <eris ?dataspace> $dataspace [

  ; require the daemon when immutulating
  $dataspace ? <Observe <rec immutulation _> _> [
    $config <require-service <daemon immutulator>>
  ]

  ; resolve  <immutulation-yt-dlp …> from <immutulation …>
  $dataspace ? <Observe <rec immutulation-yt-dlp [<lit ?url> _ _ _]> _> [

    ; temporarily start yt-dlp
    let ?id = <yt-dlp $url>
    $config <require-service <daemon $id>>
    ; translate the output of yt-dlp --dump-json
    $config ? <built json_translator ?translator _> [
      <daemon $id {
          argv: [
              $translator
              "yt-dlp"
              "--embed-subs"
              "--embed-metadata"
              "--dump-json"
              "--no-simulate"
              "--output" "/tmp/%(id)s.%(ext)s"
              $url
            ]
          protocol: application/syndicate
        }>
    ]

    $config ? <service-object <daemon $id> ?cap> [
      ; grap the filename out of the received JSON data
      $cap ? <recv {filename: ?filename}> [
        $config <require-service <daemon immutulator>>
        ; ask the real immutulator
        $dataspace ? <immutulation $filename ?eris ?size ?mime> [
          ; answer the initial observation
          $dataspace <immutulation-yt-dlp $url $eris $size $mime>
        ]
      ]
    ]

  ]

  $config [
    ? <service-object <daemon immutulator> ?cap> [
      $cap {
        cachefile: "/srv/immutulator.pr"
        dataspace: $dataspace
        stores: [ "coap+tcp://[::1]:5683" ]
      }
    ]
  ]
]
```

And I use [syndump](https://git.syndicate-lang.org/ehmry/syndicate_utils#syndump) as a frontend:
```
$ SYNDICATE_STEP=$(mintsturdyref '"eris"' < /dev/null) syndump '<immutulation-yt-dlp "https://www.youtube.com/watch?v=anwy2MPT5RE" ? ? ?>'

# Output:
+ "urn:eris:B4A3BWB3NL3AG7JLQZJ3DEOH7G6FYG6LHFH2QTOV5VUEYTMK6NWTQEYVPP2JT2VN5YXXYLKKXR3MOF35DXFKTBOVTR55Z23JQO56D2S4MM" 6030153 video/webm
```
