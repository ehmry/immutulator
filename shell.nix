let
  syndicate = builtins.getFlake "syndicate";
  pkgs =
    import <nixpkgs> { overlays = (builtins.attrValues syndicate.overlays); };
in pkgs.nimPackages.buildNimPackage (finalAttrs: prevAttrs: {
  pname = "immutulator";
  version = "unstable";
  nativeBuildInputs = [ pkgs.pkg-config ];
  propagatedBuildInputs = [ pkgs.nimPackages.getdns ];
})
