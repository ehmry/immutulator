
import
  preserves

type
  BootArgs* {.preservesDictionary.} = object
    `cachefile`*: string
    `dataspace`* {.preservesEmbedded.}: Preserve[void]
    `stores`*: seq[string]

  Immutulation* {.preservesRecord: "immutulation".} = object
    `filePath`*: string
    `erisCap`*: string
    `length`*: BiggestInt
    `mimetype`*: Symbol

proc `$`*(x: BootArgs | Immutulation): string =
  `$`(toPreserve(x))

proc encode*(x: BootArgs | Immutulation): seq[byte] =
  encode(toPreserve(x))
